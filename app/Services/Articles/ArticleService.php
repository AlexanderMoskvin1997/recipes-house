<?php

namespace App\Services\Articles;

use App\Models\Article;

class ArticleService
{
    public function getArticles()
    {
        $articles = Article::all();

        return $articles;
    }

    public function getArticle($slug)
    {
        $article = Article::where('slug',$slug)->first();

        return $article;
    }
}
