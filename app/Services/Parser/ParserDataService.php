<?php

namespace App\Services\Parser;

use App\Models\Category;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Exception\UnknownErrorException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class ParserDataService
{
    public int $countPost = 0;
    protected const WB_POST = 'https://www.simplyrecipes.com/dessert-recipes-5091513';

    /**
     * @throws UnknownErrorException
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function startParser()
    {
        putenv('WEBDRIVER_CHROME_DRIVER=C:\chromedriver.exe');

        $chromeOptions = new ChromeOptions();

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $chromeOptions);
        $driver = ChromeDriver::start($capabilities);

        sleep(3);

        $driver->get(self::WB_POST);
        sleep(10);

        try {
            var_dump('start');
            $data = [];

            $categories = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-child-block__links'))->findElements(WebDriverBy::className("mntl-text-link"));

            $i = 0;
            foreach ($categories as $category) {
                $i++;
                $data[$i]['name'] = $category->getText();
                $data[$i]['link'] = $category->getAttribute('href');
            }

            var_dump($data);

            $this->categories($driver, $data, 0);

        } catch (\Exception|Throwable $exception) {
            DB::rollBack();
        }

        $driver->quit();
    }

    protected function categories(WebDriver $driver, $data, $parentCategory): void
    {


        foreach ($data as $categoryData) {

            $elementsPath = [];
            $driver->get($categoryData['link']);
            sleep(2);
            var_dump('start');
            var_dump($categoryData['name']);
            $data = [];


            if (!$this->isBlock($driver)) {
                $elements = $driver->findElement(WebDriverBy::id('mntl-taxonomysc-article-list_1-0'))->findElements(WebDriverBy::tagName('a'));

                foreach ($elements as $element) {
                    $elementsPath[] = $element->getAttribute('href');
                }
            }

            if ($this->isBlock($driver)) {
                $i = 0;
                $categories = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-child-block__links'))->findElements(WebDriverBy::className("mntl-text-link"));
                var_dump('b');
                foreach ($categories as $category) {
                    $i++;
                    $data[$i]['name'] = $category->getText();
                    $data[$i]['link'] = $category->getAttribute('href');
                }
            }

            $name = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-heading'))->getText();
            $description = $driver->findElement(WebDriverBy::className('mntl-sc-block-html'))->getText();
            $url = $driver->findElements(WebDriverBy::className('card__img'));
            $parentCategory = $categoryData['name'];

            var_dump('aaaaaaaaaaaaa');
            $this->recipes($driver, $elementsPath, $categoryData['name']);

            var_dump('Собрали данные');
            var_dump('Название категории ' . $name);
            var_dump('Описание категории ' . $description);
            var_dump('Родительская категория ' . $parentCategory);

            var_dump('Отправка');

            if ($this->isBlock($driver) != '') {
                $this->categories($driver, $data, $parentCategory);
            }

        }
    }

    protected function recipes(WebDriver $driver, $elementsPath, $parentCat): bool
    {
        var_dump('bbbbbbbbbbbbb');
        foreach ($elementsPath as $elementPath) {

            $detailsText = [];
            $categoriesText = [];
            $tagsText = [];
            $starsText = [];
            $photoUrl = [];
            $name = '';
            $ingridientText = [];
            $stepsText = [];
            $nutritionsText = [];

            $driver->get($elementPath);
            sleep(3);

            try {
                var_dump('start');

                sleep(2);

                $title = $driver->findElement(WebDriverBy::id('heading_1-0'))->findElement(WebDriverBy::className('heading__title'))->getText();
                var_dump('title = ' . $title);
                $description = $driver->findElement(WebDriverBy::id('heading_1-0'))->findElement(WebDriverBy::className('heading__subtitle'))->getText();

                $imageBludo = $driver->findElement(WebDriverBy::id('figure_1-0'))
                    ->findElement(WebDriverBy::tagName('img'))->getAttribute('src');
                var_dump('imageBludo = ' . $imageBludo);

                $category = $parentCat;
                var_dump('Parent Category = ' . $category);
                $time = $driver->findElement(WebDriverBy::id('meta-text_1-0'))->getText();

                $porcia = $driver->findElement(WebDriverBy::id('meta-text_6-0'))->getText();

                $ingridients = $driver->findElement(WebDriverBy::id('structured-ingredients_1-0'))->findElements(WebDriverBy::tagName('li'));
                $ingridients[0]->getLocationOnScreenOnceScrolledIntoView();
                foreach ($ingridients as $ingridient) {
                    $ingridientText[] = $ingridient->getText();
                    var_dump('ingridient = ' . $ingridient->getText());
                }

                $steps = $driver->findElement(WebDriverBy::id('structured-project__steps_1-0'))->findElements(WebDriverBy::tagName('li'));

                $stepCount = 0;
                foreach ($steps as $step) {
                    $stepCount++;
                    $step->getLocationOnScreenOnceScrolledIntoView();
                    sleep(1);
                    $stepsText[$stepCount]['title'] = $step->findElement(WebDriverBy::className('mntl-sc-block-subheading__text'))->getText();
                    $stepsText[$stepCount]['description'] = $step->findElement(WebDriverBy::className('mntl-sc-block-html'))->getText();
                    if ($this->isBlockStepImg($step)) {
                        $stepsText[$stepCount]['img_url'] = $step->findElements(WebDriverBy::className('mntl-sc-block-universal-image'))[0]
                            ->findElement(WebDriverBy::className('figure-media'))
                            ->findElement(WebDriverBy::className('universal-image__image'))
                            ->getAttribute('src');
                    }
                    var_dump('stepsTitle = ' . $stepsText[$stepCount]['title']);
                    var_dump('stepsDesc = ' . $stepsText[$stepCount]['description']);

                }

                $nutritions = $driver->findElement(WebDriverBy::id('nutritional-guidelines-block_6-0'))->findElement(WebDriverBy::tagName('tbody'))->findElements(WebDriverBy::className('nutrition-info__table--row'));

                $nutritionCount = 0;
                foreach ($nutritions as $nutrition) {

                    sleep(1);
                    $nutritionCount++;
                    $nutritionsText[$nutritionCount] = $nutrition->findElements(WebDriverBy::className('nutrition-info__table--cell'))[0]->getText();
                }

                var_dump('porcia = ' . $porcia);
                var_dump('Собрали данные');

                var_dump('Отправка');

                $this->countPost++;
                var_dump('countPost = ' . $this->countPost);
                $res = Http::asForm()->post('https://recipes-house.ru/api/parser', [
                        "title" => $title,
                        "description" => $description,
                        "imageBludo" => $imageBludo,
                        "category" => $category,
                        "time" => $time,
                        "porcia" => $porcia,
                        "ingridients" => $ingridientText,
                        "steps" => $stepsText,
                        "nutritions" => $nutritionsText,
                    ]
                );

                var_dump(json_decode($res->body()));

            } catch (\Exception|Throwable $exception) {
                DB::rollBack();
            }

        }

        return true;
    }


    protected function isBlock(WebDriver $driver): bool|string
    {
        try {
            return $driver->findElement(WebDriverBy::id('mntl-taxonomysc-child-block_1-0'))->getText();
        } catch (\Exception|Throwable $exception) {
            return false;
        }
    }

    protected function isBlockCat(WebDriver $driver): bool|string
    {
        try {
            return $driver->findElement(WebDriverBy::className('tag-nav__list'))->findElements(WebDriverBy::className('tag-nav__item'))[0]->getText();
        } catch (\Exception|Throwable $exception) {
            return false;
        }
    }

    protected function isBlockStepImg($step)
    {
        try {
            return $step->findElements(WebDriverBy::className('mntl-sc-block-universal-image'))[0]
                ->findElement(WebDriverBy::className('figure-media'))
                ->findElement(WebDriverBy::className('universal-image__image'))
                ->getAttribute('src');
        } catch (\Exception|Throwable $exception) {
            return false;
        }
    }
}
