<?php

namespace App\Services\Parser;

use App\Models\Category;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Exception\UnknownErrorException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class CategoriesTranslateService
{
    protected const WB_POST = 'https://www.deepl.com/translator#en/ru/';

    /**
     * @throws UnknownErrorException
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function startParser()
    {
        putenv('WEBDRIVER_CHROME_DRIVER=C:\chromedriver.exe');

        $chromeOptions = new ChromeOptions();

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $chromeOptions);
        $driver = ChromeDriver::start($capabilities);

        sleep(3);

        $driver->get(self::WB_POST);
        sleep(10);

        $categories = Http::asForm()->get('https://recipes-house.ru/api/parser/categories');
        $recipes = Http::asForm()->get('https://recipes-house.ru/api/parser/recipes');
        $instructions = Http::asForm()->get('https://recipes-house.ru/api/parser/instructions');

        $categories = json_decode($categories->body());
        $recipes = json_decode($recipes->body());
        $instructions = json_decode($instructions->body());

 //       foreach ($categories as $category) {
 //          var_dump($category->name_en);
 //           try {
 //               var_dump('start');
 //               $title = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys(mb_strtolower($category->name_en));
 //               sleep(3);
 //               $titleTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
 //               var_dump($titleTrans);
 //               $driver->findElement(WebDriverBy::cssSelector('div[class="lmt__clear_text_button_wrapper"]'))->click();
 //               sleep(1);
//
 //               var_dump('title = '.$titleTrans);
 //               var_dump('category->id = '.$category->id);
//
 //               $res = Http::asForm()->post('https://recipes-house.ru/api/parser-trans/category', [
 //                       'category_id'=>$category->id,
 //                       'name' => $titleTrans,
 //                       'description' => $category->description
 //                   ]
 //               );
//
 //               var_dump(json_decode($res->body()));
//
 //           } catch (\Exception|Throwable $exception) {
 //               DB::rollBack();
 //           }
 //       }

//        foreach ($recipes as $recipe) {
//            $ingredientsText = [];
//            try {
//                var_dump('start');
//
//                $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys(mb_strtolower($recipe->title));
//                sleep(3);
//                $titleTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
//                $driver->findElement(WebDriverBy::className('lmt__clear_text_button_wrapper'))->click();
//
//                $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys($recipe->description);
//                sleep(5);
//                $descriptionTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
//                $driver->findElement(WebDriverBy::className('lmt__clear_text_button_wrapper'))->click();
//
//                var_dump('a');
//                var_dump(json_decode($recipe->ingredients));
//                $ingredients = json_decode($recipe->ingredients);
//                foreach ($ingredients as $ingredient) {
//                    $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys($ingredient);
//                    sleep(3);
//                    $ingredientsTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
//                    $ingredientsText[] = $ingredientsTrans;
//                    $driver->findElement(WebDriverBy::className('lmt__clear_text_button_wrapper'))->click();
//                }
//
//                var_dump('title = '.$titleTrans);
//
//                $res = Http::asForm()->post('https://recipes-house.ru/api/parser-trans/recipe', [
//                        'recipe_id'=>$recipe->id,
//                        'title' => $titleTrans,
//                        'ingredients' => $ingredientsText,
//                        'description' => $descriptionTrans
//                    ]
//                );
//
//                var_dump(json_decode($res->body()));
//
//            } catch (\Exception|Throwable $exception) {
//                DB::rollBack();
//            }
//        }
//
        foreach ($instructions as $instruction) {
            try {

                var_dump('start');

                $title = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys($instruction->title);
                sleep(3);
                $titleTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
                $driver->findElement(WebDriverBy::className('lmt__clear_text_button_wrapper'))->click();

                $description = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-source-input"]'))->sendKeys($instruction->description);
                sleep(5);
                $descriptionTrans = $driver->findElement(WebDriverBy::cssSelector('d-textarea[dl-test="translator-target-input"]'))->getText();
                $driver->findElement(WebDriverBy::className('lmt__clear_text_button_wrapper'))->click();


                var_dump('title = '.$titleTrans);
                var_dump('description = '.$descriptionTrans);

                $res = Http::asForm()->post('https://recipes-house.ru/api/parser-trans/instruction', [
                        'instruction_id'=>$instruction->id,
                        'title' => $titleTrans,
                        'description' => $descriptionTrans
                    ]
                );

                var_dump(json_decode($res->body()));

            } catch (\Exception|Throwable $exception) {
                DB::rollBack();
            }
        }
    }
}
