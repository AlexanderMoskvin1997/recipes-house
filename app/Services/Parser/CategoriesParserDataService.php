<?php

namespace App\Services\Parser;

use App\Models\Category;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Exception\UnknownErrorException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriver;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;

class CategoriesParserDataService
{
    protected const WB_POST = 'https://www.simplyrecipes.com/recipes-5090746';

    /**
     * @throws UnknownErrorException
     * @throws NoSuchElementException
     * @throws TimeoutException
     */
    public function startParser()
    {
        putenv('WEBDRIVER_CHROME_DRIVER=C:\chromedriver.exe');

        $chromeOptions = new ChromeOptions();

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY_W3C, $chromeOptions);
        $driver = ChromeDriver::start($capabilities);

        sleep(3);

        $driver->get(self::WB_POST);
        sleep(10);

        try {
            var_dump('start');
            $data = [];

            $categories = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-child-block__links'))->findElements(WebDriverBy::className("mntl-text-link"));

            $i = 0;
            foreach ($categories as $category) {
                $i++;
                $data[$i]['name'] = $category->getText();
                $data[$i]['link'] = $category->getAttribute('href');
            }

            var_dump($data);

            $this->categories($driver, $data,0);

        } catch (\Exception|Throwable $exception) {
            DB::rollBack();
        }

        $driver->quit();
    }

    protected function categories(WebDriver $driver, $data,$parentCategory): void
    {

        foreach ($data as $categoryData) {

            $driver->get($categoryData['link']);
            sleep(3);
            var_dump('start');
            var_dump($categoryData['name']);
            $data = [];

            if ($this->isBlock($driver)) {
                $i = 0;
                $categories = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-child-block__links'))->findElements(WebDriverBy::className("mntl-text-link"));
                var_dump('b');
                foreach ($categories as $category) {
                    $i++;
                    $data[$i]['name'] = $category->getText();
                    $data[$i]['link'] = $category->getAttribute('href');
                }
            }

            $name = $driver->findElement(WebDriverBy::className('mntl-taxonomysc-heading'))->getText();
            $description = $driver->findElement(WebDriverBy::className('mntl-sc-block-html'))->getText();
            $url = $driver->findElements(WebDriverBy::className('card__img'));

            var_dump('Собрали данные');
            var_dump('Название категории ' . $name);
            var_dump('Описание категории ' . $description);
            var_dump('Родительская категория ' . $parentCategory);

            var_dump('Отправка');
            $res = Http::asForm()->post('https://recipes-house.ru/api/parser-categories', [
                    "name" => $categoryData['name'],
                    "url" => $url[0]->getAttribute('src'),
                    "parent" => $parentCategory,
                    "description" => $description,

                ]
            );

            var_dump(json_decode($res->body()));

            if ($this->isBlock($driver) != '') {
                $this->categories($driver, $data,$categoryData['name']);
            }

        }
    }

    protected function isBlock(WebDriver $driver)
    {
        try {
            return $driver->findElement(WebDriverBy::id('mntl-taxonomysc-child-block_1-0'))->getText();
        } catch (\Exception|Throwable $exception) {
            return false;
        }
    }

    protected function createPhotoCategory(Category $cat, $url): bool
    {
        $file = file_get_contents($url);

        $destination_path = 'categories-avatar/' . $cat->id . '/' . md5($file . time()) . '.png';
        Storage::deleteDirectory('categories-avatar/' . $cat->id);
        Storage::put($destination_path, $file);
        $cat->image = $destination_path;
        $cat->save();

        return true;
    }
}
