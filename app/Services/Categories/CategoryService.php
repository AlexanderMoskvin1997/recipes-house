<?php

namespace App\Services\Categories;

use App\Models\Article;
use App\Models\Category;

class CategoryService
{
    public function getCategories()
    {
        $categories = Category::where('parent_id', '0')->get();

        return $categories;
    }

    public function getCategory($slug)
    {
        $category = Category::where('slug', '=', $slug)->first();

        return $category;
    }

    public function getSubcategories($slug)
    {
        $subcategories = Category::where('parent_id', '=', $this->getCategory($slug)->id)->get();

        return $subcategories;
    }
}
