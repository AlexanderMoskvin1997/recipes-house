<?php

namespace App\Services\Recipes;

use App\Models\Category;
use App\Models\Instruction;
use App\Models\Recipe;
use App\Models\RecipeCategory;
use Illuminate\Support\Facades\DB;
use Throwable;

class RecipeService
{

    public function getRecipes($slug)
    {
        global $recipeCollect;
        $recipeCollect = Recipe::query();

        $category = Category::where('slug', $slug)->first();


            if ($categoryParent = Category::where('parent_id', $category->id)->first()) {
                $parentCats = Category::where('parent_id', $category->id)->get();
                foreach ($parentCats as $parentCat) {
                    if ($categoryParent = Category::where('parent_id', $parentCat->id)->first()) {
                        $parentCatss = Category::where('parent_id', $parentCat->id)->get();
                        foreach ($parentCatss as $parentCatt) {
                            if ($categoryParent = Category::where('parent_id', $parentCatt->id)->first()) {
                                $parentCatsss = Category::where('parent_id', $parentCatt->id)->get();
                                foreach ($parentCatsss as $parentCattt) {
                                    if (!Category::where('parent_id', $parentCattt->id)->first()) {
                                        $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                                            $query->with('recipe');
                                        })->whereHas('recipeCategories', function ($query) use ($parentCattt) {
                                            $query->where('category_id', $parentCattt->id);
                                        }));
                                    }
                                }
                            } else {
                                $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                                    $query->with('recipe');
                                })->whereHas('recipeCategories', function ($query) use ($parentCatt) {
                                    $query->where('category_id', $parentCatt->id);
                                }));
                            }
                        }
                    } else {
                        $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                            $query->with('recipe');
                        })->whereHas('recipeCategories', function ($query) use ($parentCat) {
                            $query->where('category_id', $parentCat->id);
                        }));
                    }
                }
            } else {
                $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                    $query->with('recipe');
                })->whereHas('recipeCategories', function ($query) use ($category) {
                    $query->where('category_id', $category->id);
                }));
            }

        return $recipeCollect;
    }

    public function getRecipe($slug)
    {
        $recipe = Recipe::where('slug', '=', $slug)->first();

        return $recipe;
    }

    public function getSteps($slug)
    {
        $steps = Instruction::where('recipe_id', '=', $this
            ->getRecipe($slug)->id)
            ->orderBy('step', 'asc')
            ->get();

        return $steps;
    }

    public function getRecomendation($slug)
    {
        $recipe_id = $this->getRecipe($slug)->id;
        $category_id = RecipeCategory::where('recipe_id', $recipe_id)->first()->category_id;
        $recipes = Recipe::query()->with('recipeCategories', function ($query) {
            $query->with('recipe');
        })->whereHas('recipeCategories', function ($query) use ($category_id) {
            $query->where('category_id', $category_id);
        })->limit(12)->get();

        return $recipes;
    }

//    protected function recipesPerentCat($recipeCollect, Category $category)
//    {
//        if ($categoryParent = Category::where('parent_id', $category->id)->first()) {
//            $parentCats = Category::where('parent_id', $category->id)->get();
//            foreach ($parentCats as $parentCat) {
//                return $this->recipesPerentCat($recipeCollect, $parentCat);
//            }
//
//        } else {
//           $recipeCat = Recipe::query()->with('recipeCategories', function ($query) {
//                $query->with('recipe');
//            })->whereHas('recipeCategories', function ($query) use ($category) {
//                $query->where('category_id', $category->id);
//            })->get();
//
//            if ($recipeCat !== null) {
//                $recipeCollect = $recipeCollect->merge($recipeCat);
//            }
//
//        }
//
//        return $recipeCollect;
//    }
}
