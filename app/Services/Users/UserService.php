<?php

namespace App\Services\Users;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Throwable;

class UserService
{
    public function getUser(int $id): User
    {
        return User::findOrFail($id);
    }

    /**
     * @param array $data
     * @return User
     * @throws Throwable
     */
    public function store(array $data): User
    {
        try {
            DB::beginTransaction();

            if (!$user = User::create($data)) {
               return false;
            }

            DB::commit();

            return $user;

        } catch (Throwable $ex) {
            DB::rollBack();
            throw $ex;
        }
    }
}
