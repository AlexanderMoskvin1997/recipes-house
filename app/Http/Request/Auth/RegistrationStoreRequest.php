<?php

namespace App\Http\Request\Auth;


use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class RegistrationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'unique:users,email'],
            'password' => [
                'required',
                'string',
                'min:' . config('app.user.password.min'),
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Необходимо указать имя',
            'email.required' => 'Необходимо указать Email',
            'email.unique' => 'Пользователь с таким Email уже зарегистрирован',
            'password.required' => 'Необходимо указать пароль',
            'password.min' => 'Пароль должен состоять минимум из ' . config('app.user.password.min') . ' символов'
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @throws ValidationException
     */
    public function authenticate(): void
    {
        $user = $this->getCurrentUser();

        if (!$user) {
            throw ValidationException::withMessages([
                'email' => 'Пользователь с таким Email уже зарегистрирован',
            ]);
        } else {
            Auth::login($user);
        }
    }

    public function getCurrentUser(): User|null
    {
        return User::where(['email' => $this->input('email')])
            ->first();
    }
}

