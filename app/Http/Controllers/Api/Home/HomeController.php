<?php

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;

class HomeController extends Controller
{
    public function index()
    {

        $articles = \App\Models\Article::query()->with('articleCategories', function ($query) {
            $query->with('category');
        })->get();

        $recipes = \App\Models\Category::query()->with('recipeCategories', function ($query) {
            $query->with('category');
        })->get()->random(20);

        return [$articles, $recipes];
    }
}
