<?php

namespace App\Http\Controllers\Api\Parser;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Instruction;
use App\Models\Recipe;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;


class CategoriesTransController extends Controller
{
    function transCategories(Request $request): JsonResponse
    {
        $data = $request->request->all();

        try {

            DB::beginTransaction();

            $category = Category::where('id',$data['category_id'])->first();
            $category->update([
                'name' => $data['name'],
                'description' => $data['description']
            ]);
            $category->save();

            DB::commit();
            return response()->json(new JsonResponse(
                'Перевод выполнен',
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'Перевод не выполнен : ' . $ex->getMessage(),
            ));
        }
    }

    function transRecipe(Request $request): JsonResponse
    {
        $data = $request->request->all();
        try {
            DB::beginTransaction();

            $recipe = Recipe::where('id',$data['recipe_id'])->first();
            $recipe->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'ingredients' => json_encode($data['ingredients']),
            ]);
            $recipe->save();

            DB::commit();
            return response()->json(new JsonResponse(
                'Перевод выполнен',
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'Перевод не выполнен : ' . $ex->getMessage(),
            ));
        }
    }

    function transInstruction(Request $request): JsonResponse
    {
        $data = $request->request->all();
        try {
            DB::beginTransaction();

            $instruction = Instruction::where('id',$data['instruction_id'])->first();
            $instruction->update([
                'title' => $data['title'],
                'description' => $data['description']
            ]);
            $instruction->save();

            DB::commit();
            return response()->json(new JsonResponse(
                'Перевод выполнен',
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'Перевод не выполнен : ' . $ex->getMessage(),
            ));
        }
    }
}
