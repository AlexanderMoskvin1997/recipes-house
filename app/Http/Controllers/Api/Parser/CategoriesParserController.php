<?php

namespace App\Http\Controllers\Api\Parser;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Throwable;


class CategoriesParserController extends Controller
{
    function createCategories(Request $request): JsonResponse
    {
        $data = $request->request->all();
        try {
            DB::beginTransaction();

            if (Category::where('name_en', $data['name'])->first()) {
                return response()->json(new JsonResponse(
                    'Категория уже существует ',
                ));
            }

            $parent_id = !is_null(Category::where('name_en','=',$data['parent'])->first())
            ? Category::where('name_en','=',$data['parent'])->first()->id
            : 0;

            $cat =  Category::create([
                'slug' => Str::slug($data['name']),
                'name' => $data['name'],
                'name_en' => $data['name'],
                'description' => $data['description'],
                'parent_id' => $parent_id,
                'type' => 1,
            ]);

            $this->createPhotoCategory($cat,$data['url']);

            DB::commit();

            return response()->json(new JsonResponse(
                'Категория сохранена',
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'Категория не добавлена : ' . $ex->getMessage(),
            ));
        }
    }

    protected function createPhotoCategory(Category $cat, $url): bool
    {
        $file = file_get_contents($url);

        $destination_path = 'categories-avatar/' . $cat->id . '/' . md5($file . time()) . '.png';
        Storage::deleteDirectory('categories-avatar/' . $cat->id);
        Storage::put($destination_path, $file);
        $cat->image = $destination_path;
        $cat->save();

        return true;
    }
}
