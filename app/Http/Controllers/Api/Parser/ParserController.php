<?php

namespace App\Http\Controllers\Api\Parser;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Instruction;
use App\Models\Recipe;
use App\Models\RecipeCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;

class ParserController extends Controller
{

    public function getCategories(): Collection
    {
        return Category::all();
    }

    public function getRecipes(): Collection
    {
        return Recipe::all();
    }

    public function getInstructions(): Collection
    {
        return Instruction::all();
    }

    function createPost(Request $request)
    {
        $data = $request->request->all();
        $faker = Faker::create();

        try {
            DB::beginTransaction();

            if ($this->postExists($data)) {
                return response()->json(new JsonResponse(
                    'Рецепт не добавлен. Рецепт уже существует ',
                ));
            }
            if (Category::where('name_en', '=', $data['category'])->first()->parent_id != 0) {
                $recipe = Recipe::create([
                    'slug',
                    'title' => $data['title'],
                    'title_en' => $data['title'],
                    'description' => $data['description'],
                    'description_en' => $data['description'],
                    'ingredients' => json_encode($data['ingridients']),
                    'calories' => $data['nutritions'][1],
                    'proteins' => $data['nutritions'][4],
                    'fats' => $data['nutritions'][2],
                    'carbs' => $data['nutritions'][3],
                    'time' => $data['time'],
                    'portions' => $data['porcia'],

                ]);

                $this->createPhotoRecipe($recipe, $data['imageBludo']);


                RecipeCategory::create([
                    'category_id' => Category::where('name_en', '=', $data['category'])->first()->id,
                    'recipe_id' => $recipe->id,
                ]);

                $v = 0;
                foreach ($data['steps'] as $step) {
                    $v++;
                    $instruction = Instruction::create([
                        'recipe_id' => $recipe->id,
                        'title' => $step['title'],
                        'description' => $step['description'],
                        'step' => $v,
                    ]);

                    if (array_key_exists('img_url', $step)) {
                        $this->createPhotoInstruction($instruction, $step['img_url']);
                    }
                }

            }
            DB::commit();

            return response()->json(new JsonResponse(
                'Рецепт добавлено',
            ));

        } catch (Throwable $ex) {
            DB::rollBack();
            return response()->json(new JsonResponse(
                'Рецепт не добавлен : ' . $ex->getMessage(),
            ));
        }
    }

    protected function postExists($data)
    {
        $post = Recipe::where('title_en', $data['title'])->first();

        return !($post == '');
    }

    protected function createPhotoRecipe(Recipe $recipe, $url): bool
    {
        $file = file_get_contents($url);

        $destination_path = 'recipes-avatar/' . $recipe->id . '/' . md5($file . time()) . '.png';
        Storage::deleteDirectory('recipes-avatar/' . $recipe->id);
        Storage::put($destination_path, $file);
        $recipe->image = $destination_path;
        $recipe->save();

        return true;
    }

    protected function createPhotoInstruction(Instruction $instruction, $url): bool
    {
        $file = file_get_contents($url);

        $destination_path = 'instruction-image/' . $instruction->id . '/' . md5($file . time()) . '.png';
        Storage::deleteDirectory('instruction-image/' . $instruction->id);
        Storage::put($destination_path, $file);
        $instruction->image = $destination_path;
        $instruction->save();

        return true;
    }

}
