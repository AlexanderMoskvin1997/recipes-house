<?php

namespace App\Http\Controllers\Api\Search;

use App\Filters\Recipe\RecipeFilter;
use App\Http\Controllers\Controller;
use App\Models\Recipe;

class SearchController extends Controller
{
    /**
     * @param RecipeFilter $recipeFilter
     */
    public function index($search)
    {
        $search = str_replace('  ', '', $search);
        $search = trim($search);

        $recipes = Recipe::where(function ($query) use ($search) {
            $query->where('title', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%')
                ->orWhereHas('recipeCategories', function ($query) use ($search) {
                    $query->whereHas('category', function ($query) use ($search) {
                        $query->where('name', 'like', '%' . $search . '%')
                            ->orWhere('name_en', 'like', '%' . $search . '%');
                    });
                });
        })->paginate(12);

        return $recipes;
    }
}
