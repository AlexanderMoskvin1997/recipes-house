<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Recipe;

class CatalogController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', '=', 0)->paginate(12);

        return $categories;
    }

    public function show($slug)
    {
        global $recipeCollect;
        $recipeCollect = collect();

        $category = Category::where('slug', $slug)->first();
        $recipeCollect = Recipe::whereHas('recipeCategories', function ($query) use ($category) {
            $query->where('category_id', $category->id);
        });

        if ($categoryParent = Category::where('parent_id', $category->id)->first()) {
            $parentCats = Category::where('parent_id', $category->id)->get();
            foreach ($parentCats as $parentCat) {
                if ($categoryParent = Category::where('parent_id', $parentCat->id)->first()) {
                    $parentCatss = Category::where('parent_id', $parentCat->id)->get();
                    foreach ($parentCatss as $parentCatt) {
                        if ($categoryParent = Category::where('parent_id', $parentCatt->id)->first()) {
                            $parentCatsss = Category::where('parent_id', $parentCatt->id)->get();
                            foreach ($parentCatsss as $parentCattt) {
                                if (!Category::where('parent_id', $parentCattt->id)->first()) {
                                    $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                                        $query->with('recipe');
                                    })->whereHas('recipeCategories', function ($query) use ($parentCattt) {
                                        $query->where('category_id', $parentCattt->id);
                                    }));
                                }
                            }
                        } else {
                            $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                                $query->with('recipe');
                            })->whereHas('recipeCategories', function ($query) use ($parentCatt) {
                                $query->where('category_id', $parentCatt->id);
                            }));
                        }
                    }
                } else {
                    $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                        $query->with('recipe');
                    })->whereHas('recipeCategories', function ($query) use ($parentCat) {
                        $query->where('category_id', $parentCat->id);
                    }));
                }
            }
        } else {
            $recipeCollect->union(Recipe::query()->with('recipeCategories', function ($query) {
                $query->with('recipe');
            })->whereHas('recipeCategories', function ($query) use ($category) {
                $query->where('category_id', $category->id);
            }));
        }

        return $recipeCollect->paginate(12);
    }
}
