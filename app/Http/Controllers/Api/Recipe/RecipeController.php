<?php

namespace App\Http\Controllers\Api\Recipe;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;

class RecipeController extends Controller
{
    public function index()
    {
        $crecipes = Category::query()->with('recipeCategories', function ($query) {
            $query->with('article');
        })->paginate();

        return $categories;
    }
}
