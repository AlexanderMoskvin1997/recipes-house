<?php

namespace App\Http\Controllers\Web;

use App\Filters\Recipe\CategoryFilter;
use App\Filters\Recipe\RecipeFilter;
use App\Http\Controllers\Controller;
use App\Models\Recipe;
use Illuminate\View\View;

class SearchController extends Controller
{
    /**
     * @param RecipeFilter $recipeFilter
     * @return View
     */
    public function index(RecipeFilter $recipeFilter): View
    {
        $recipes = Recipe::filter($recipeFilter)->get();

        return view('home.search')->with(['recipes' => $recipes, 'q' => $_REQUEST['q']]);
    }
}
