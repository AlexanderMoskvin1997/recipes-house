<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Categories\CategoryService;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public function __construct(protected CategoryService $service)
    {

    }
}
