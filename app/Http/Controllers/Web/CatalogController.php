<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Recipe;
use App\Services\Categories\CategoryService;
use App\Services\Recipes\RecipeService;
use Illuminate\View\View;

class CatalogController extends Controller
{
    public function __construct(protected CategoryService $categoryService, protected RecipeService $recipeService)
    {

    }

    public function index(): View
    {
        return view('catalog.index')->with([
            'categories' => $this->categoryService->getCategories()
        ]);
    }

    public function show($slug): View
    {
        $categoryId = Category::where('slug', $slug)->first()->id;

        $recipes = Recipe::where(function ($query) use ($categoryId) {
            $query->orWhereHas('recipeCategories', function ($query) use ($categoryId) {
                    $query->where('category_id', '=', $categoryId);
            });
        })->paginate(12);

        return view('catalog.show')->with([
            'category' => $this->categoryService->getCategory($slug),
            'subcategories' =>$this->categoryService->getSubcategories($slug),
            'recipes' => $this->recipeService->getRecipes($slug)
        ]);
    }
}
