<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use App\Services\Articles\ArticleService;
use Illuminate\View\View;

class ArticleController extends Controller
{
    public function __construct(protected ArticleService $service)
    {

    }

    public function index(): View
    {
        return view('article.index')->with([
            'articles' => $this->service->getArticles()
        ]);
    }

    public function show($slug): View
    {
        return view('article.show')->with([
            'article' => $this->service->getArticle($slug)
        ]);
    }
}
