<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Recipes\RecipeService;
use Illuminate\View\View;

class RecipeController extends Controller
{
    public function __construct(protected RecipeService $service)
    {

    }

    public function show($slug): View
    {
        return view('recipe.show')->with([
            'recipe' => $this->service->getRecipe($slug),
            'steps' => $this->service->getSteps($slug),
            'recomendations' => $this->service->getRecomendation($slug),
        ]);
    }
}
