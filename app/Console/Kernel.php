<?php

namespace App\Console;

use App\Console\Commands\CategoriesParserData;
use App\Console\Commands\CategoriesSlug;
use App\Console\Commands\ParserData;
use App\Console\Commands\ParserPostSEO;
use App\Console\Commands\SitemapGenerate;
use App\Console\Commands\TranslateCategories;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        ParserData::class,
        CategoriesSlug::class,
        CategoriesParserData::class,
        ParserPostSEO::class,
        SitemapGenerate::class,
        TranslateCategories::class,
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
