<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Recipe;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\CommonMark\Util\Xml;
use SimpleXMLElement;

class SitemapGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap generate';

    protected function arrayToXml ($array, $rootElement = null, $xml = null) {
        $_xml = $xml;
        if ($_xml === null) {
            $_xml = new SimpleXMLElement ($rootElement !== null ? $rootElement : '<urlset/>');
        }

        foreach ($array as $k => $v) {
            if(is_int($k))
            {
                $k='url';
                $v=$v['url'];
            }
            if (is_array($v)) {
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            }
            else {

                $_xml->addChild($k, $v);
            }
        }
        return $_xml->asXML();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = [];
        $dt = Carbon::now();
        echo $dt->toDateString();

        $recipes = Recipe::all();
        $this->output->progressStart(count($recipes));

        foreach ($recipes as $recipe) {
            $data[] = ['url' => [
                'loc' => 'https://recipes-house.ru/recipes/' . $recipe->slug,
                'lastmod' => $dt->toDateString(),
            ]];

            $this->output->progressAdvance();
        }
        $this->output->progressFinish();

        Storage::put('sitemap.csv', $this->arrayToXml($data));

        return 0;
    }
}
