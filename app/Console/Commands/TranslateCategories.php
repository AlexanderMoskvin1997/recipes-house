<?php

namespace App\Console\Commands;

use App\Services\Parser\CategoriesTranslateService;
use Illuminate\Console\Command;

class TranslateCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:trans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Categories translate';

    /**
     * @var CategoriesTranslateService
     */
    protected CategoriesTranslateService $categoriesTranslateService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CategoriesTranslateService $categoriesTranslateService)
    {
        $this->categoriesTranslateService = $categoriesTranslateService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->categoriesTranslateService->startParser();
        $this->comment('Парсинг закончен');
        return 0;
    }
}
