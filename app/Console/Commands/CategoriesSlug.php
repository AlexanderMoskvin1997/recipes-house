<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Recipe;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CategoriesSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Categories slug';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = Category::all();
        $recipes = Recipe::all();
        $this->output->progressStart(count($categories));

        foreach ($categories as $category) {

            $category->slug = Str::slug($this->name , "-");
            $category->save();
            $this->output->progressAdvance();
        }


        $this->output->progressFinish();

        $this->output->progressStart(count($recipes));

        foreach ($recipes as $recipe) {

            $recipe->slug = Str::slug($this->name , "-");
            $recipe->save();
            $this->output->progressAdvance();
        }


        $this->output->progressFinish();

        return 0;
    }
}
