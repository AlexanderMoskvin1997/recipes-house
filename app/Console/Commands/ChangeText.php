<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Recipe;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ChangeText extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:text';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change text';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $recipes = Recipe::all();

        foreach ($recipes as $recipe) {
            $recipe->proteins = str_replace('g', ' гр.', $recipe->proteins);
            $recipe->fats = str_replace('g', ' гр.', $recipe->fats);
            $recipe->carbs = str_replace('g', ' гр.', $recipe->carbs);
            $recipe->time = str_replace('mins', 'мин.', $recipe->time);
            $recipe->time = str_replace('TOTAL TIME', '', $recipe->time);
            $recipe->portions = str_replace(['SERVINGS', 'servings'], '', $recipe->portions);

            $recipe->save();
        }

        return true;
    }
}
