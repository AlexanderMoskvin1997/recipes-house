<?php

namespace App\Console\Commands;

use App\Services\Parser\ParserDataService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ParserData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser run';

    /**
     * @var ParserDataService
     */
    protected ParserDataService $parserDataService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ParserDataService $parserDataService)
    {
        $this->parserDataService = $parserDataService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->parserDataService->startParser();
        $this->comment('Парсинг закончен');
        return 0;
    }
}
