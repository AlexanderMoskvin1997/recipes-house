<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Services\Parser\ParserDataService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ParserPostSEO extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:seo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parser SEO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts = Post::all();
        $this->output->progressStart(count($posts));
        foreach ($posts as $post) {
            $post->update([
                'seo_title' => $post->title,
                'seo_description' => $post->title,
                'seo_keywords' => implode(' ', json_decode($post->tags)),
            ]);
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        $this->comment('Парсинг закончен');
        return 0;
    }
}
