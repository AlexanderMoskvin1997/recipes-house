<?php

namespace App\Console\Commands;

use App\Services\Parser\CategoriesParserDataService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CategoriesParserData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'categories_parser:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Categories parser run';

    /**
     * @var CategoriesParserDataService
     */
    protected CategoriesParserDataService $categoriesParserDataService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CategoriesParserDataService $categoriesParserDataService)
    {
        $this->categoriesParserDataService = $categoriesParserDataService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->categoriesParserDataService->startParser();
        $this->comment('Парсинг закончен');
        return 0;
    }
}
