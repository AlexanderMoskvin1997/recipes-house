<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $title_en
 * @property string $description
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 *  @property-read ArticleCategory[]|Collection $article_categories
 *  @property-read Instruction[]|Collection $instructions
 */
class Article extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'slug',
        'title',
        'title_en',
        'description',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public function setSlugAttribute() {
        $this->attributes['slug'] = Str::slug($this->title , "-");
    }

    public function articleCategories(): HasMany
    {
        return $this->hasMany(ArticleCategory::class, 'article_id', 'id');
    }

    public function instructions(): HasMany
    {
        return $this->hasMany(Instruction::class, 'article_id', 'id');
    }
}
