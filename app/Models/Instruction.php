<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property int $recipe_id
 * @property string $title
 * @property string $description
 * @property int $step
 * @property string $image
 *
 *  @property-read Recipe $recipe
 */
class Instruction extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'recipe_id',
        'title',
        'description',
        'step',
        'image',
    ];

    public function recipe(): HasMany
    {
        return $this->hasMany(Recipe::class, 'recipe_id', 'id');
    }
}
