<?php

namespace App\Models;

use App\Filters\Filterable;
use App\Filters\Orderable;
use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $title_en
 * @property string $description
 * @property string $description_en
 * @property string image
 * @property string $calories
 * @property string $proteins
 * @property string $fats
 * @property string $carbs
 * @property string $time
 * @property string $portions
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 * @method static Builder|Recipe filter(QueryFilter $filter)
 * @property-read Category $category
 * @property-read Recipe[]|Collection $recipe_categories
 * @property-read UserRecipe[]|Collection $user_recipes
 */
class Recipe extends Model
{
    use HasApiTokens, HasFactory, Notifiable, Filterable, Orderable;

    protected $fillable = [
        'slug',
        'title',
        'title_en',
        'description',
        'description_en',
        'image',
        'ingredients',
        'calories',
        'proteins',
        'fats',
        'carbs',
        'time',
        'portions',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public function setSlugAttribute()
    {
        $this->attributes['slug'] = Str::slug($this->title, "-");
    }

    public function recipeCategories(): HasMany
    {
        return $this->hasMany(RecipeCategory::class, 'recipe_id', 'id');
    }

    public function userRecipes(): HasMany
    {
        return $this->hasMany(UserRecipe::class, 'recipe_id', 'id');
    }
}
