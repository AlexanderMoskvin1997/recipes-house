<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $name_en
 * @property string $description
 * @property int $parent_id
 * @property int $type
 * @property string $image
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 *
 * @property-read ArticleCategory[]|Collection $articleCategories
 * @property-read RecipeCategory[]|Collection $recipeCategories
 */
class Category extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'slug',
        'name',
        'name_en',
        'description',
        'parent_id',
        'image',
        'type',
        'seo_title',
        'seo_description',
        'seo_keywords',
    ];

    public function setSlugAttribute() {
        $this->attributes['slug'] = Str::slug($this->name , "-");
    }

    public function articleCategories(): HasMany
    {
        return $this->hasMany(ArticleCategory::class, 'category_id', 'id');
    }

    public function recipeCategories(): HasMany
    {
        return $this->hasMany(RecipeCategory::class, 'category_id', 'id');
    }
}
