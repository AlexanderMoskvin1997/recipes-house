<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

/**
 * @method static Builder order(QueryOrder $order)
 *
 */
trait Orderable
{
    public function scopeOrder(Builder $builder, QueryOrder $order)
    {
        $order->apply($builder);
    }
}
