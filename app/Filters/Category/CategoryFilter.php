<?php

namespace App\Filters\Recipe;

use App\Filters\QueryFilter;


class CategoryFilter extends QueryFilter
{
    public function q(string $q)
    {
        $this->builder->where(function ($query) use ($q) {
            $query->where('title', 'like', '%' . $q . '%')
                ->orWhere('description', 'like', '%' . $q . '%')
                ->orWhereHas('recipeCategories', function ($query) use ($q) {
                    $query->whereHas('category', function ($query) use ($q) {
                        $query->where('name', 'like', '%' . $q . '%')
                            ->orWhere('name_en', 'like', '%' . $q . '%');
                    });
                });
        });
    }
}
