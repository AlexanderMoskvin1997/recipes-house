<?php

namespace App\Enums;

class UserEnum
{
    const ADMIN = 0;
    const USER = 1;
}
