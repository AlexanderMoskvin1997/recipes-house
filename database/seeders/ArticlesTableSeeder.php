<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ArticlesTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;

    public function run()
    {
        $faker = Faker::create();
        $categories = Category::all();

        foreach ($categories as $category) {
            for ($i = 0; $i < self::NUM_ITEMS; $i++) {

                $article = Article::create([
                    'title' => $faker->title,
                    'description' => $faker->text,
                ]);

                $article->update([
                    'slug' => Str::slug($article->title, "-"),
                ]);

                ArticleCategory::create([
                    'category_id' => $category->id,
                    'article_id' => $article->id,
                ]);
            }
        }
    }

}
