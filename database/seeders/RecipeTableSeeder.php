<?php

namespace Database\Seeders;

use App\Models\Instruction;
use App\Models\Recipe;
use App\Models\RecipeCategory;
use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RecipeTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;

    public function run()
    {
        $faker = Faker::create();

        $categories = Category::all();

        foreach ($categories as $category) {
            for ($i = 0; $i < self::NUM_ITEMS; $i++) {

                $recipe = Recipe::create([
                    'title' => $faker->text(50),
                    'description' => $faker->text(150),
                    'image' => asset('img/default-img/eda.jpg'),
                    'ingredients' => json_encode([
                        '80 г риса',
                        '120 мл молока',
                        '100 мл воды',
                        '1 ст.л.сахара',
                        'щепотка соли',
                        '1 среднее яблоко',
                        '1 яйцо',
                        '2 ст.л.топленого',
                        'масла для жарки'
                    ]),
                    'calories' => '350кл',
                    'proteins' => '25г',
                    'fats' => '50г',
                    'carbs' => '43г',
                    'time' => '20',
                    'portions' => '5'
                ]);

                $recipe->update([
                    'slug' => Str::slug($recipe->title, "-"),
                ]);

                RecipeCategory::create([
                    'category_id' => $category->id,
                    'recipe_id' => $recipe->id,
                ]);
                $randomCount = rand(1, 6);
                for ($a = 1; $a < $randomCount; $a++) {
                    Instruction::create([
                        'recipe_id' => $recipe->id,
                        'title' => $faker->text(100),
                        'description' => $faker->text(100),
                        'step' => $a,
                        'image' => asset('img/default-img/eda.jpg'),
                    ]);
                }
            }
        }
    }

}
