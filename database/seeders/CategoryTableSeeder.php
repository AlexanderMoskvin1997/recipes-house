<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryTableSeeder extends Seeder
{
    const NUM_ITEMS = 10;

    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < self::NUM_ITEMS; $i++) {

          $category =  Category::create([
                'name' => $faker->title,
                'description' => "Welcome to our depot for all things drink. You'll find refreshing summer lemonade and
                spritzers, steaming hot chocolate, and healthy smoothies. Mixing up a cocktail? We have plenty of those,
                 too, with and without alcohol. Bottom's up!",
                'parent_id' => !is_null(Category::all()->first()) ? Category::all()->first()->id : 0,
                'image' => asset('img/default-img/eda.jpg'),
            ]);

            $category->update([
                'slug'=>Str::slug($category->title , "-"),
            ]);
        }
    }

}
