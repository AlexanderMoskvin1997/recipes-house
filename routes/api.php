<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('')->name('api.')->group(function () {
    require __DIR__ . '/api/home.php';
    require __DIR__ . '/api/recipe.php';
    require __DIR__ . '/api/search.php';
    require __DIR__ . '/api/parser-categories.php';
    require __DIR__ . '/api/parser.php';
    require __DIR__ . '/api/catalog.php';
    require __DIR__ . '/api/parser-trans.php';
});
