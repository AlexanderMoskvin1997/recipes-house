<?php

use App\Http\Controllers\Web\ArticleController;
use Illuminate\Support\Facades\Route;

Route::prefix('articles')->name('articles.')->group(function () {

    Route::get('', [ArticleController::class, 'index'])
        ->name('index');

    Route::get('{slug}', [ArticleController::class, 'show'])
        ->name('show');
});
