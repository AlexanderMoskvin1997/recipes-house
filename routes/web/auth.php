<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegistrationController;
use Illuminate\Support\Facades\Route;

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->name('login.store');

Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->name('logout');


Route::get('/register', [RegistrationController::class, 'create'])
    ->name('register');

Route::post('/register/store', [RegistrationController::class, 'store'])
    ->name('register.store');
