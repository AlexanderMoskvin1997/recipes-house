<?php

use App\Http\Controllers\Web\RecipeController;
use Illuminate\Support\Facades\Route;

Route::prefix('recipes')->name('recipes.')->group(function () {

    Route::get('{slug}', [RecipeController::class, 'show'])
        ->name('show');
});
