<?php

use App\Http\Controllers\Api\Parser\CategoriesParserController;
use App\Http\Controllers\Api\Parser\CategoriesTransController;
use Illuminate\Support\Facades\Route;

Route::prefix('parser-trans')->name('parser-trans.')->group(function () {

    Route::post('category', [CategoriesTransController::class, 'transCategories'])
        ->name('transCategories');

    Route::post('recipe', [CategoriesTransController::class, 'transRecipe'])
        ->name('transRecipe');

    Route::post('instruction', [CategoriesTransController::class, 'transInstruction'])
        ->name('transInstruction');
});
