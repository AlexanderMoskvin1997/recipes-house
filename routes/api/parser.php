<?php

use App\Http\Controllers\Api\Parser\ParserController;
use Illuminate\Support\Facades\Route;

Route::prefix('parser')->name('parser.')->group(function () {

    Route::post('', [ParserController::class, 'createPost'])
        ->name('store');

    Route::get('categories', [ParserController::class, 'getCategories'])
        ->name('categories');

    Route::get('recipes', [ParserController::class, 'getRecipes'])
        ->name('recipes');

    Route::get('instructions', [ParserController::class, 'getInstructions'])
        ->name('instructions');
});
