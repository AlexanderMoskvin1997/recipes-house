<?php

use App\Http\Controllers\Api\Recipe\RecipeController;
use Illuminate\Support\Facades\Route;

Route::prefix('recipe')
    ->name('recipe.')
    ->group(function () {

        Route::get('', [RecipeController::class, 'index'])
            ->name('index');
    });
