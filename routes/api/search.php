<?php

use App\Http\Controllers\Api\Catalog\CatalogController;
use Illuminate\Support\Facades\Route;

Route::prefix('search')
    ->name('search.')
    ->group(function () {

        Route::get('{search}', [CatalogController::class, 'index'])
            ->name('index');
    });
