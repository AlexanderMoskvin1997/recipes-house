<?php

use App\Http\Controllers\Api\Parser\CategoriesParserController;
use Illuminate\Support\Facades\Route;

Route::prefix('parser-categories')->name('parser-categories.')->group(function () {

    Route::post('', [CategoriesParserController::class, 'createCategories'])
        ->name('createCategories');
});
