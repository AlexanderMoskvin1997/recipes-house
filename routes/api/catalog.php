<?php

use App\Http\Controllers\Api\Catalog\CatalogController;
use Illuminate\Support\Facades\Route;

Route::prefix('catalog')->name('catalog.')->group(function () {

    Route::get('', [CatalogController::class, 'index'])
        ->name('index');

    Route::get('{slug}', [CatalogController::class, 'show'])
        ->name('show');
});
