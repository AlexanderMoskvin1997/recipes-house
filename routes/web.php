<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::prefix('')->name('web.')->group(function () {
    require __DIR__ . '/web/home.php';
    require __DIR__ . '/web/auth.php';
    require __DIR__ . '/web/article.php';
    require __DIR__ . '/web/recipe.php';
    require __DIR__ . '/web/catalog.php';
    require __DIR__ . '/web/search.php';
});
