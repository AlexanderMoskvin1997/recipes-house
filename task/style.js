const {src, dest, watch} = require('gulp');

const sass = require('gulp-sass')(require('sass'));

const clean = require('gulp-clean-css');
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const map = require('gulp-sourcemaps');
const bulkSass = require('gulp-sass-bulk-import');

module.exports = function style() {
    return src([
        './resources/sass/app.scss',
    ])
        .pipe(map.init())
        .pipe(bulkSass())
        .pipe(sass().on('error', sass.logError))
        .pipe(clean({level: 2}))
        .pipe(concat('app.css'))
        .pipe(minifyCSS())
        .pipe(dest('./public/css'))
};




