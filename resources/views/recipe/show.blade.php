@extends('master.guest')

@section('meta-title')
{{$recipe->title}}, рецепты с фото на | Recipes-house.ru
@endsection

@section('meta-descriptions')
{{$recipe->description}}
@endsection

@section('meta-pagename')
Рецепт {{$recipe->title}}
@endsection

@section('meta-keywords')
{{$recipe->seo_keywords}}
@endsection

@section('title')
{{$recipe->title}}, рецепты с фото на | Recipes-house.ru
@endsection


@section('content')
    <recipe-show
        :recipe="{{$recipe}}"
        :steps="{{$steps}}"
        :recomendations="{{$recomendations}}"
        route_recipe_show="{{route('web.recipes.show', ':slug')}}"
    ></recipe-show>
@endsection
