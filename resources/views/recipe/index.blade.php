@extends('master.guest')

@section('title')
 Рецепты
@endsection

@section('content')
    <recipes-main
        route_show="{{route('web.recipes.show',":slug")}}"
        route_api="/api/recipe"
    ></recipes-main>
@endsection
