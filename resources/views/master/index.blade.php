<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="title" content="@yield('meta-title')">
    <meta name="description" content="@yield('meta-descriptions')">
    <meta name="keywords" content="@yield('meta-keywords')">
    <meta name="yandex-verification" content="8c6a34365732b120" />
    <meta name="pagename" content="@yield('meta-pagename')" />

     <!--Logo-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
    <link rel="mask-icon" href="{{asset('img/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!--Logo-->

    <link rel="stylesheet" href={{ asset('css/app.css') }}>

    <script type="text/javascript" src="{{ asset('js/vendor/jquery/jquery-3.6.0.min.js') }}"></script>
    <script type="stylesheet" src="{{ asset('js/vendor/slick-1.8.1/slick/slick.css') }}"></script>
    <script type="stylesheet" src="{{ asset('js/vendor/slick-1.8.1/slick/slick-theme.css') }}"></script>

    <title>@yield('title')</title>

    @stack('assets')

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();
            for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(92746414, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/92746414" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Yandex.RTB -->
    <script>window.yaContextCb=window.yaContextCb||[]</script>
    <script src="https://yandex.ru/ads/system/context.js" async></script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7DSJKPEH32"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-7DSJKPEH32');
    </script>

</head>

<body>
<div id="app">
    <main>
        @yield('content-master')
    </main>
</div>

<script src="{{ asset('js/app.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/slick-1.8.1/slick/slick.min.js') }}"></script>
</body>
</html>
