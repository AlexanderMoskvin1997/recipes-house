@extends('master.index')

@section('content-master')
    @include ('master.layouts.header')
    @yield('content')
    @include ('master.layouts.footer')
@endsection
