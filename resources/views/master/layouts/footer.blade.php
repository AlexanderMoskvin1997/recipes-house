@php
$dt = \Carbon\Carbon::now();
@endphp

<footer-main
    current_year={{$dt->year}}
    route_home="{{route('web.home.index')}}"
    >
</footer-main>
