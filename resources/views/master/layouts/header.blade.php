@php
    $randomArticles = \App\Models\Article::all();
    $randomRecipesCat = \App\Models\Category::all()->random(10);
    $authUser = auth()->user();
@endphp

<header-main
    route_login="{{route('web.login')}}"
    route_search="{{route('web.search.index')}}"
    route_logout="{{route('web.logout')}}"
    route_register="{{route('web.register')}}"
    route_categories="{{route('web.home.index')}}"
    route_article="{{route('web.articles.show',":slug")}}"
    route_recipe="{{route('web.recipes.show',":slug")}}"
    route_catalog="{{route('web.catalog.index')}}"
    route_catalog_show="{{route('web.catalog.show',":slug")}}"
    is_main_page="{{url()->current() == route('web.home.index')}}"
    route_home="{{route('web.home.index')}}"
    search="{{($_REQUEST['q']) ?? ''}}"
    :random_articles="{{$randomArticles}}"
    :random_recipes_cat="{{$randomRecipesCat}}"
    auth_user="{{$authUser}}"
>
</header-main>
