@extends('master.guest')

@section('title')
 Каталог рецептов
@endsection

@section('content')
    <catalog-main
        route_category_show="{{route('web.catalog.show', ':slug')}}"
        route_api="{{'/api/catalog'}}"
        current_url="{{url()->current()}}"
        main_page="{{route('web.home.index')}}"
    ></catalog-main>
@endsection
