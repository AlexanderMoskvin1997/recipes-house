@extends('master.guest')

@section('meta-title')
{{$category->name}}, рецепты с фото на | Recipes-house.ru
@endsection

@section('meta-descriptions')
{{$category->description}}
@endsection

@section('meta-pagename')
Рецепты {{$category->name}}
@endsection

@section('meta-keywords')
{{$category->seo_keywords}}
@endsection

@section('title')
{{$category->name}}, рецепты с фото на | Recipes-house.ru
@endsection


@section('content')
    <catalog-show
        route_category_show="{{route('web.catalog.show', ':slug')}}"
        route_recipe_show="{{route('web.recipes.show', ':slug')}}"
        :category="{{$category}}"
        :subcategories="{{$subcategories}}"
        route_api="{{'/api/catalog/' . $category->slug}}"
        main_page="{{route('web.home.index')}}"
        route_catalog="{{route('web.catalog.index')}}"
    ></catalog-show>
@endsection
