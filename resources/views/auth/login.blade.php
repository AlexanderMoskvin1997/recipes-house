@extends('master.guest')

@section('title')
    Войти
@endsection

@section('content')
    <div class="auth">
        <form class="auth__form" action="{{route('web.login.store')}}"
              method="POST">
            @method('POST')
            @csrf

            <div class="input  @error("email") invalid @enderror">
                <label class="input__label" for="email">Email</label>
                <input id="email" class="input-text" type="email" name="email" placeholder="Введите Email">
                @error("email")
                <span class="invalid-feedback" role="alert">
              <p class="invalid-feedback--p">{{ $message }}</p>
              </span>
                @enderror
            </div>

            <div class="input  @error("password") invalid @enderror">
                <label class="input__label" for="password">Пароль</label>
                <input id="password" class="input-text" type="password" name="password" placeholder="Введите пароль">
                @error("password")
                <span class="invalid-feedback" role="alert">
              <p class="invalid-feedback--p">{{ $message }}</p>
              </span>
                @enderror
            </div>


            <div class="auth__buttons">
                <button class="btn auth__submit" type="submit">Войти</button>
                <a href="{{route('web.home.index')}}" class="btn auth__back">Отмена</a>
            </div>
        </form>
    </div>
@endsection
