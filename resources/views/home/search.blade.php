@extends('master.guest')

@section('title')
    Поиск
@endsection

@section('meta-pagename')
    Поиск
@endsection

@section('content')
    <search-main
        route_recipe_show="{{route('web.recipes.show', ":slug")}}"
        route_api="{{'/api/search/'.($_REQUEST['q'])??''}}"
        current_url="{{url()->current()}}"
    ></search-main>
@endsection
