@extends('master.guest')

@section('meta-title')
Recipes-house.ru
@endsection

@section('meta-descriptions')
Recipes-house.ru это надежный ресурс для домашних поваров с более чем 3000 проверенных рецептов
@endsection

@section('meta-pagename')
Recipes-house.ru
@endsection

@section('meta-keywords')
рецепты,домашние блюда,как приготовить,рецепты с фото,вкусно,рецепт,
@endsection

@section('title')
Recipes-house.ru
@endsection

@section('content')
    <home-main
        route_article_show="{{route('web.articles.show',":slug")}}"
        route_recipe_show="{{route('web.recipes.show',":slug")}}"
        route_catalog_show="{{route('web.catalog.show',":slug")}}"
        route_api="/api/home"
    ></home-main>
@endsection
