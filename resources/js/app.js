import './bootstrap';
import Vue from "vue";

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

library.add(faChevronDown)

window.Vue = require('vue');

Vue.component('header-main', require('./components/layouts/Header.vue').default);
Vue.component('footer-main', require('./components/layouts/Footer.vue').default);
Vue.component('home-main', require('./components/home/Home.vue').default);
Vue.component('catalog-main', require('./components/catalog/CatalogMain.vue').default);
Vue.component('catalog-show', require('./components/catalog/CatalogShow.vue').default);
Vue.component('recipes-main', require('./components/recipes/Recipes.vue').default);
Vue.component('search-main', require('./components/search/SearchMain.vue').default);
Vue.component('recipe-show', require('./components/recipes/RecipeShow.vue').default);
Vue.component('font-awesome-icon', FontAwesomeIcon);

const app = new Vue({
    el: '#app'
});
